allen
=====

About
-----

The project aims to engineer the ontology of intertextual relations in the field of movies. This is achieved by leveraging domain expert knowledge and automated information extraction from semi-structured and unstructured data.

Screenshots
----------
![alt text](https://gitlab.com/knoweng/allen/raw/master/media/screenshots/allen_1.png "allen ontology Protege screenshot 1")
![alt text](https://gitlab.com/knoweng/allen/raw/master/media/screenshots/allen_2.png "allen ontology Protege screenshot 2")
